﻿using System;
using System.Collections.Generic;

namespace A52Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            var navegador = new Navegador();

            navegador.NavegarPara("google.com");
            navegador.NavegarPara("caelum.com.br");
            navegador.NavegarPara("alura.com.br");

            navegador.Anterior();
            navegador.Anterior();
            navegador.Anterior();

            navegador.Proximo();

            Console.ReadLine();
        }
    }

    internal class Navegador
    {
        private readonly Stack<string> historicoAnterior = new Stack<string>();
        private readonly Stack<string> historicoPosterior = new Stack<string>();
        private string atual = "vazia";

        public Navegador()
        {
            ImprimirAtual();
        }

        internal void Anterior()
        {
            if (historicoAnterior.Count > 0)
            {
                historicoPosterior.Push(atual);
                atual = historicoAnterior.Pop();
                ImprimirAtual();
            }
        }

        internal void NavegarPara(string url)
        {

            historicoAnterior.Push(atual);
            atual = url;
            ImprimirAtual();
        }

        internal void Proximo()
        {
            if (historicoPosterior.Count > 0)
            {
                historicoAnterior.Push(atual);
                atual = historicoPosterior.Pop();
                ImprimirAtual();
            }
        }

        private void ImprimirAtual()
        {
            Console.WriteLine("Página atual: " + atual);
        }
    }
}
